import { Construct, RemovalPolicy, Stack, StackProps } from '@aws-cdk/core';
import { Bucket } from '@aws-cdk/aws-s3';
import { BucketDeployment, Source } from '@aws-cdk/aws-s3-deployment';

export class ChronoFriezeStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const webAppBucket = new Bucket(this, "CfrWebappBucket", {
      bucketName: "chrono.afradet.fr",
      publicReadAccess: true,
      removalPolicy: RemovalPolicy.DESTROY,
      websiteIndexDocument: "index.html"
    });

    // Front deployment
    new BucketDeployment(this, "CfrWebappDeploy", {
      sources: [Source.asset("../front/dist/chrono-frieze")],
      destinationBucket: webAppBucket
    });

    //   const periodTable = new Table(this,  "chrono-frieze-periods", {
    //     tableName: "periods",
    //     partitionKey: {
    //       name: "periodId",
    //       type: AttributeType.STRING
    //     },
    //     removalPolicy: RemovalPolicy.DESTROY
    //   });
    //
    //   const customEnv: CustomEnv = {
    //     'TABLE_NAME': periodTable.tableName,
    //     'PRIMARY_KEY': 'periodId'
    // };

    // const getAllPeriods = new Function(this, 'getAllPeriodsFunction', this.buildLambdaProp('fr.afradet.chronofrieze.serverlessapp.handler.GetAllPeriods', customEnv));
    // const addNewPeriod = new Function(this, 'addNewPeriodFunction', this.buildLambdaProp('fr.afradet.chronofrieze.serverlessapp.handler.AddNewPeriod', customEnv));
    //
    // periodTable.grantReadWriteData(getAllPeriods);
    // periodTable.grantReadWriteData(addNewPeriod);
    //
    // const gateway = new RestApi(this, 'periodApi', {
    //   restApiName: "Periods Service",
    //   description: "This service manage crud operations for periods",
    // });
    //
    // const periodResource = gateway.root.addResource("periods");
    //
    // // TODO : check email delivery system through SES
    // const userPool = new UserPool(this, "chrono-frieze-users", {
    //   signInAliases: { email: true, phone: false, username: false },
    //   selfSignUpEnabled: false,
    //   userVerification: { emailStyle: VerificationEmailStyle.LINK}
    // });
    //
    // //
    // const authorizer =  new CfnAuthorizer(this, 'chrono-frieze-auth', {
    //   restApiId: gateway.restApiId,
    //   name: 'ChronoFriezeAuthorize',
    //   type: 'COGNITO_USER_POOLS',
    //   identitySource: 'method.request.header.Authorization',
    //   providerArns: [userPool.userPoolArn]
    // });
    //
    // periodResource.addMethod(HttpMethods.GET, new LambdaIntegration(getAllPeriods), this.buildEndpointAuth(authorizer));
    // periodResource.addMethod(HttpMethods.POST, new LambdaIntegration(addNewPeriod), this.buildEndpointAuth(authorizer));
  }

  // buildLambdaProp(handlerName: string, customEnv: CustomEnv) {
  //   return {
  //     runtime: Runtime.JAVA_8,
  //     handler: handlerName,
  //     code: Code.fromAsset(path.join(__dirname, '../../../serverless-app/target/serverless-app-1.0-SNAPSHOT-jar-with-dependencies.jar')),
  //     timeout: Duration.seconds(30),
  //     memorySize: 512,
  //     environment: customEnv
  //   }
  // }
  //
  // buildEndpointAuth(authorizer: CfnAuthorizer): MethodOptions {
  //   return {
  //     authorizationType: AuthorizationType.COGNITO,
  //       authorizer: {
  //     authorizerId: authorizer.ref,
  //   }
  //   }
  // }
}
