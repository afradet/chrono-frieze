#!/usr/bin/env node
import 'source-map-support/register';
import { ChronoFriezeStack } from '../lib/chrono-frieze-stack';
import { App, Tags } from '@aws-cdk/core';

const app = new App();
new ChronoFriezeStack(app, 'ChronoFriezeStack', {
  env: {
    region: 'eu-central-1'
  }
});
Tags.of(app).add('afr:project', 'chrono-frieze');
Tags.of(app).add('afr:stack', 'CDK');
