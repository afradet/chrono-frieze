package fr.afradet.chronofrieze.serverlessapp.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaRuntime;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import fr.afradet.chronofrieze.serverlessapp.model.GatewayResponse;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.ScanRequest;
import software.amazon.awssdk.services.dynamodb.model.ScanResponse;

import java.util.HashMap;
import java.util.Map;

public class GetAllPeriods implements RequestHandler<Map<String,Object>, GatewayResponse> {

    public GatewayResponse handleRequest(Map<String, Object> stringObjectMap, Context context) {
        LambdaRuntime.getLogger().log("Lambda a run");

        String output = getData(context);

        LambdaRuntime.getLogger().log("Data recup : " + output);


        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/text");

        return new GatewayResponse(output, headers, 200);
    }

    private String getData(Context context) {
        DynamoDbClient ddb = DynamoDbClient.create();
        String tableName= System.getenv("TABLE_NAME");
        ScanRequest scanRequest= ScanRequest.builder()
                .tableName(tableName)
                .build();
        ScanResponse response = ddb.scan(scanRequest);
        return response.toString();
    }
}
